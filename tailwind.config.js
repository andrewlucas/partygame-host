
module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      keyframes: {
        swing: {
          "0%": { transform: "rotate(0deg)" },
          "50%": { transform: "rotate(10deg)" },
          "100%": { transform: "rotate(0deg)" },
        },
        wave: {
          "0%": { transform: "translateY(0)" },
          "50%": { transform: "translateY(-10rem)" },
          "100%": { transform: "translateY(0)" },
        },
      },
      animation: {
        swing: "swing 1s ease-in-out infinite",
        wave: "wave 1s ease-in-out infinite",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [

  ],
};
