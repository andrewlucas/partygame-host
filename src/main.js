import { createApp } from "vue";
import App from "./App.vue";
import "./index.css";
import socket from "./plugins/socket";
import { createPinia } from "pinia";

const pinia = createPinia();

createApp(App)
  .use(pinia)
  .use(socket, {
    // connection: "192.168.2.106:5000",
    // connection: "https://jogandojuntos.site",
    connection: import.meta.env.VITE_SERVER_URL,

    options: {
      // add token to handshake
      auth: (cb) =>
        cb({ token: sessionStorage.getItem("token") || "", isHost: true }),
    },
  })
  .mount("#app");
