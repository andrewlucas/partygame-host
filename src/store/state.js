import { defineStore } from "pinia";

export const useStateStore = defineStore({
  id: "state",
  state: () => ({
    room: {
      players: [],
    },
    game: {},
    handleMessage: null,
    notifications: [],
  }),
  actions: {},
});
